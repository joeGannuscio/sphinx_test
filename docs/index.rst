.. Sphinx Test documentation master file, created by
   sphinx-quickstart on Thu Jun 11 11:38:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx Test's documentation!
=======================================

.. include:: modules.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
