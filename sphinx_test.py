def main():
    """The main method."""
    print(calculate_sum(1, 1))

def calculate_sum(a, b):
    """
    This function returns the sum of 2 numbers.

    :param a: first number
    :param b: second number

    :return: sum of a and b
    """
    return (a+b)

if __name__ == '__main__':
    main()
